#Slimy

A pygame based game coded in python.

##Install Instructions
Run main.py

Linux:
In your terminal type - "python main.py"

Windows:
In IDLE open up the script and hit F5.

##Screenshot
![view](http://markroxor.pythonanywhere.com/static/snakes.png)

Reference and Acknowledgement:
Tutorials by Bucky.
